- What directive is used by Node.js in loading the modules it needs?
	Answer: using the 'require' directive or require()

- What Node.js module contains a method for server creation?
	Answer: createServer() method

- What is the method of the http object responsible for creating a server using Node.js?
	Answer: http.createServer((request, response) => {})

- What method of the response object allows us to set status codes and content types?
	Answer: response.writeHead()

- Where will console.log() output its contents when run in Node.js?
	Answer: terminal

- What property of the request object contains the address's endpoint?
	Answer: url