const http = require("http");

// Create a variable "port" to store the port number
const port = 4000;

// Create a variable called "server" that stores the output of the "createServer()" method
const server = http.createServer((request, response) => {
	// Accessing the 'greeting' route that returns a message of "Hello Again"
	// http://localhost:4000/greeting
	// "request" is an object that is sent via the client(browser)
	// The "url" property refers to the url or the link in the browswer
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello Again')
	} else if(request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('This is the homepage')
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page Not Available')
	}
});

server.listen(port);

// When server is running, console will print the message
console.log(`Server now accessible at localhost:${port}`);